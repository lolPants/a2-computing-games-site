// Package Dependencies
const morgan = require('morgan')
const ensure = require('connect-ensure-login')
const session = require('express-session')
const passport = require('passport')
const TwitterStrategy = require('passport-twitter').Strategy

// Express App
const express = require('express')
const app = express()

// Environment Variables
const { CONSUMER_KEY, CONSUMER_SECRET, CALLBACK_URL, EXPRESS_SECRET } = process.env

// Passport
passport.use(new TwitterStrategy({
  consumerKey: CONSUMER_KEY,
  consumerSecret: CONSUMER_SECRET,
  callbackURL: CALLBACK_URL || 'http://localhost:3000/auth/twitter/callback',
}, (token, secret, profile, callback) => callback(null, profile)))

// User Store
passport.serializeUser((user, callback) => callback(null, user))
passport.deserializeUser((user, callback) => callback(null, user))

// Server Middleware
app.use(morgan('combined'))
app.use(session({ secret: EXPRESS_SECRET, resave: false, saveUninitialized: false }))
app.use(passport.initialize())
app.use(passport.session())

// Login Route
app.get('/auth/twitter', passport.authenticate('twitter'))
app.get('/auth/twitter/callback', passport.authenticate('twitter', { failureRedirect: '/' }), (req, res) => {
  req.session.user = req.user
  res.redirect(req.session.returnTo || '/')
})

// Logout Route
app.get('/auth/logout', (req, res) => {
  req.session.user = null
  res.redirect('/')
})

// Ensure Login Function
let ensureLogin = ensure.ensureLoggedIn('/auth/twitter')

app.get('/', (req, res) => {
  res.send('<a href="/auth/twitter">Login</a>')
})

app.get('/secure', ensureLogin, (req, res) => {
  res.send(req.session.user)
})

// Start Server
app.listen(process.env.PORT || 3000)
