/**
 * Token String [X / O / E]
 * @typedef {string} Token
 */

/**
 * X Token
 * @type {Token}
 */
const X = 'X'

/**
 * O Token
 * @type {Token}
 */
const O = 'O'

/**
 * Empty Token
 * @type {Token}
 */
const E = '-'

class TicTacToe {
  constructor () {
    this.board = {
      0: { 0: E, 1: E, 2: E },
      1: { 0: E, 1: E, 2: E },
      2: { 0: E, 1: E, 2: E },
    }
  }

  static get tokens () {
    return { X, O, E }
  }

  /**
   * Play a token on the board
   * @param {number} x X-Coordinate
   * @param {number} y Y-Coordinate
   * @param {Token} token Token to play
   * @param {boolean} [force] Force the token to be played
   * @private
   * @returns {TicTacToe}
   */
  _playToken (x, y, token, force = false) {
    let pos = this.board[y][x]
    if (pos !== E && !force) throw new Error('Cannot play a token in a non-empty place')
    else this.board[y][x] = token
    return this
  }

  /**
   * Play an X token on the board
   * @param {number} x X-Coordinate
   * @param {number} y Y-Coordinate
   * @param {boolean} [force] Force the token to be played
   * @returns {TicTacToe}
   */
  playX (x, y, force) { return this._playToken(x, y, X, force) }

  /**
   * Play a O token on the board
   * @param {number} x X-Coordinate
   * @param {number} y Y-Coordinate
   * @param {boolean} [force] Force the token to be played
   * @returns {TicTacToe}
   */
  playO (x, y, force) { return this._playToken(x, y, O, force) }

  /**
   * Checks an object for a win state
   * @param {Object} obj Object to check
   * @param {Token} token Token
   * @returns {boolean|Token}
   */
  _checkOBJ (obj, token) {
    if (
      obj[0] === token &&
      obj[1] === token &&
      obj[2] === token
    ) { return token } else { return false }
  }

  /**
   * Checks a row for a win state.
   * Returns false if no-one has won.
   * Returns a token string if there is a win.
   * @param {number} row Row Number
   * @private
   * @returns {boolean|Token}
   */
  _checkRow (row) {
    if (row < 0 || row > 2) throw new Error('Row out of bounds')
    let rowOBJ = this.board[row]

    if (this._checkOBJ(rowOBJ, X)) return X
    else if (this._checkOBJ(rowOBJ, O)) return O
    else return false
  }

  /**
   * Checks a column for a win state.
   * Returns false if no-one has won.
   * Returns a token string if there is a win.
   * @param {number} col Column Number
   * @private
   * @returns {boolean|Token}
   */
  _checkColumn (col) {
    if (col < 0 || col > 2) throw new Error('Column out of bounds')
    let colOBJ = {
      0: this.board[0][col],
      1: this.board[1][col],
      2: this.board[2][col],
    }

    if (this._checkOBJ(colOBJ, X)) return X
    else if (this._checkOBJ(colOBJ, O)) return O
    else return false
  }

  /**
   * Checks both diagonals for win states.
   * Returns false if no-one has won.
   * Returns a token string if there is a win.
   * @private
   * @returns {boolean|Token}
   */
  _checkDiag () {
    let diag1OBJ = {
      0: this.board[0][0],
      1: this.board[1][1],
      2: this.board[2][2],
    }
    let diag2OBJ = {
      0: this.board[2][0],
      1: this.board[1][1],
      2: this.board[0][2],
    }

    if (this._checkOBJ(diag1OBJ, X)) return X
    else if (this._checkOBJ(diag1OBJ, O)) return O
    else if (this._checkOBJ(diag2OBJ, X)) return X
    else if (this._checkOBJ(diag2OBJ, O)) return O
    else return false
  }

  /**
   * Checks for a win state.
   * Returns false if no-one has won.
   * Returns a token string if there is a win.
   * @returns {boolean|Token}
   */
  checkWin () {
    let checks = []
    for (let i = 0; i < 3; i++) { checks.push(this._checkRow(i)) }
    for (let i = 0; i < 3; i++) { checks.push(this._checkColumn(i)) }
    checks.push(this._checkDiag())

    checks = checks.filter(o => o !== false)
    return checks.length === 0 ? false : checks[0]
  }
}

module.exports = TicTacToe
