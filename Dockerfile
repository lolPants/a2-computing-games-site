FROM node:latest

# Create app directory
WORKDIR /usr/src/app

# Copy package info
COPY package.json .
COPY package.json yarn.lock ./

# Install app dependencies
RUN yarn

# Bundle app source
COPY . .

# Start node
EXPOSE 3000
CMD [ "node", "." ]